from gensim.models import Word2Vec

from bs4 import BeautifulSoup
import re
from nltk.corpus import stopwords
import numpy as np
import pickle

class SentiClassifier:
    def __init__(self):
        self.w2v = Word2Vec.load('word2vec_300size')
        self.classifier = self.load_clf('xgb_clf.pkl')
    
    def review_to_wordlist(self, review, remove_stopwords=True):
        review_text = BeautifulSoup(review, features="html.parser").get_text()
        review_text = re.sub("[^a-zA-Z]", " ", review_text)
        words = review_text.lower().split()
        if remove_stopwords:
            stops = set(stopwords.words('english'))
            words = [w for w in words if not w in stops]
        return (words)
    
    def makeFeatureVec(self, words, model, num_features):
        featureVec = np.zeros((num_features,),dtype="float32")
        nwords = 0
        index2words_set = set(model.wv.index_to_key)
        for word in words:
            if word in index2words_set:
                nwords = nwords + 1
                featureVec = np.add(featureVec, model.wv[word])
        featureVec = np.divide(featureVec, nwords)
        return featureVec
    
    def load_clf(self, clfname):
        f = open(clfname, 'rb')
        clf = pickle.load(f)
        f.close()
        return clf
    
    def sentiment_analyze(self, rawtext):
        text = self.review_to_wordlist(rawtext, remove_stopwords=True)
        vec = self.makeFeatureVec(text, self.w2v, 300).reshape((1,-1))
        sentiment = self.classifier.predict(vec)
        if sentiment==0:
            sentiment = 'negative'
        elif sentiment==1:
            sentiment = 'positive'
        return sentiment